<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Chat</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
</head>
<body>
    <div class="row" id="app">
        <div class="container">
            </br>
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    Chat Room
                </div>
                <ul class="list-group list-group-flush">
                    <message v-for="value in chat.message">
                        @{{ value }}
                    </message>
                    <input type="text" class="form-control" v-model='message' @keyup.enter='send'>
                </ul>
         </div>
                
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>